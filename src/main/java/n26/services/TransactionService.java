package n26.services;

import n26.exceptions.OutdatedTransactionException;
import n26.models.Transaction;
import n26.models.TransactionSummary;
import n26.models.TransactionSummaryCircularContainer;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
public class TransactionService {

	private TransactionSummaryCircularContainer transactionSummaryCircularContainer;

	public TransactionService() {
		this.transactionSummaryCircularContainer = new TransactionSummaryCircularContainer(Instant.now());
	}

	/**
	 * Process transaction. If the transaction is older than a minute, this method will throw the checked exception
	 * @param transaction
	 * @throws OutdatedTransactionException
	 */
	public void processTransaction(Transaction transaction) throws OutdatedTransactionException {
		if (transaction.isTimestampBefore(Instant.now().minus(1, ChronoUnit.MINUTES))) {
			throw new OutdatedTransactionException();
		}
		transactionSummaryCircularContainer.addUntil60SecondsAgo(transaction);
	}

	/**
	 * Retrieve the summary of the last 60 seconds. If no transaction happened in the last 60 seconds, this method will return a summary with values 0
	 * @param now
	 * @return
	 */
	public TransactionSummary getSummaryOfLast60Seconds(Instant now) {
		return transactionSummaryCircularContainer.getSummaryOfLast60Seconds(now);
	}

}
