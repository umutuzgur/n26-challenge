package n26.controllers;

import n26.exceptions.OutdatedTransactionException;
import n26.models.Transaction;
import n26.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/transactions")
public class TransactionController {
	private static Logger log = LoggerFactory.getLogger(TransactionController.class);

	private final TransactionService transactionService;

	@Autowired
	public TransactionController(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@RequestMapping(method = RequestMethod.POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<?> processTransaction(@RequestBody Transaction transaction) {
		try {
			transactionService.processTransaction(transaction);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (OutdatedTransactionException e) {
			log.info("Transaction is outdated");
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@ExceptionHandler
	public void logException(Exception e) throws Exception {
		log.error("Uncaught exception", e);
		throw e;
	}

}
