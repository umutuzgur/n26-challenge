package n26.controllers;

import n26.models.TransactionSummary;
import n26.services.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/statistics")
public class StatisticsController {

	private static Logger log = LoggerFactory.getLogger(StatisticsController.class);

	private final TransactionService transactionService;

	@Autowired
	public StatisticsController(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@RequestMapping(method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public TransactionSummary getSummary() {
		return transactionService.getSummaryOfLast60Seconds(Instant.now());
	}

	@ExceptionHandler
	public void logException(Exception e) throws Exception {
		log.error("Uncaught exception", e);
		throw e;
	}

}
