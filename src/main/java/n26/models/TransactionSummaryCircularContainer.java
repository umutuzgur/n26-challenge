package n26.models;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class TransactionSummaryCircularContainer {

	// TODO Set to 120 seconds slots for easier coding and testing. Decrease the number of slots later on
	private static final int SLOTS = 120;

	private final Instant start;
	// TODO Inject the list via constructor and do a mockito spy to test the number of iterations in the methods to ensure performance
	private final List<TransactionSummary> summariesInaMinute = new ArrayList<>(SLOTS);

	public TransactionSummaryCircularContainer(Instant start) {
		this.start = start;
		IntStream.range(0, SLOTS).forEach(i -> summariesInaMinute.add(new TransactionSummary(Instant.EPOCH)));
	}

	/**
	 * Add transaction to every slot within the last 60 seconds for faster access and update summaries
	 * @param transaction
	 */
	public void addUntil60SecondsAgo(Transaction transaction) {
		int endOf60Seconds = calculateCorrespondingSecondSlot(transaction.getTimestampInstant());
		for (int i = endOf60Seconds; i >= endOf60Seconds - 60; i--) {
			int correctIndex = Math.floorMod(i, SLOTS);
			TransactionSummary summary = summariesInaMinute.get(correctIndex);
			synchronized (summary) {
				if (summary.isSummaryCreatedBefore(transaction.getTimestampInstant().minus(1, ChronoUnit.MINUTES))) {
					summary.reset(transaction.getTimestampInstant());
				}
				summary.collectData(transaction);
			}
		}
	}

	/**
	 * Gets the value of the corresponding second slot 60 seconds ago
	 * @param now
	 * @return
	 */
	public TransactionSummary getSummaryOfLast60Seconds(Instant now) {
		Instant aMinuteAgo = now.minus(1, ChronoUnit.MINUTES);
		int i = calculateCorrespondingSecondSlot(aMinuteAgo);
		TransactionSummary summary = summariesInaMinute.get(i);
		synchronized (summary) {
			if (!summary.isSummaryCreatedBefore(aMinuteAgo)) {
				return summary.copy();
			} else {
				return new TransactionSummary(now);
			}
		}
	}

	private int calculateCorrespondingSecondSlot(Instant instant) {
		return Math.floorMod((int) (Duration.between(start, instant).getSeconds()), SLOTS);
	}
}
