package n26.models;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;

public class Transaction implements Serializable {

	private double amount;
	private long timestamp;

	public Transaction() {
	}

	public Transaction(double amount, long timestamp) {
		super();
		this.amount = amount;
		this.timestamp = timestamp;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public double getAmount() {
		return amount;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public Instant getTimestampInstant() {
		return Instant.ofEpochMilli(timestamp);
	}

	public boolean isTimestampBefore(Instant instant) {
		return Duration.between(getTimestampInstant(), instant).getSeconds() >= 0;
	}
}
