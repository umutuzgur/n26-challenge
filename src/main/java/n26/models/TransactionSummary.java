package n26.models;

import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;

public class TransactionSummary implements Serializable {

	private Instant creationTimestamp;
	private double sum;
	private double avg;
	private double max;
	private double min;
	private long count;

	public TransactionSummary(Instant creationTimestamp, double sum, double avg, double max, double min, long count) {
		this.creationTimestamp = creationTimestamp;
		this.sum = sum;
		this.avg = avg;
		this.max = max;
		this.min = min;
		this.count = count;
	}

	public TransactionSummary(Instant creationTimestamp) {
		this(creationTimestamp, 0, 0, 0, 0, 0);
	}

	/**
	 * Collect data from the transaction and update values
	 * @param transaction
	 */
	public void collectData(Transaction transaction) {
		synchronized (this) {
			addToSum(transaction);
			setMaxIfGreater(transaction);
			setMinIfSmaller(transaction);
			increaseCount();
			calculateAverage();
		}
	}

	private void addToSum(Transaction transaction) {
		sum += transaction.getAmount();
	}

	private void increaseCount() {
		count++;
	}

	private void setMaxIfGreater(Transaction transaction) {
		this.max = transaction.getAmount() > max ? transaction.getAmount() : max;
	}

	private void setMinIfSmaller(Transaction transaction) {
		this.min = transaction.getAmount() < min || isEmpty() ? transaction.getAmount() : min;
	}

	private boolean isEmpty() {
		return count == 0;
	}

	private void calculateAverage() {
		this.avg = sum / count;
	}

	public double getSum() {
		return sum;
	}

	public double getAvg() {
		return avg;
	}

	public double getMax() {
		return max;
	}

	public double getMin() {
		return min;
	}

	public long getCount() {
		return count;
	}

	/**
	 * Resets the values in the summary
	 * @param creationTimestamp
	 */
	public void reset(Instant creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
		this.sum = 0;
		this.avg = 0;
		this.max = 0;
		this.min = 0;
		this.count = 0;
	}

	/**
	 * Compares if the summary is created before a certain time
	 * @param instant
	 * @return
	 */
	public boolean isSummaryCreatedBefore(Instant instant) {
		return Duration.between(creationTimestamp, instant).getSeconds() > 0;
	}

	/**
	 * Creates a new fresh object with the current data
	 * @return
	 */
	public TransactionSummary copy() {
		synchronized (this) {
			return new TransactionSummary(creationTimestamp, sum, avg, max, min, count);
		}
	}

}
