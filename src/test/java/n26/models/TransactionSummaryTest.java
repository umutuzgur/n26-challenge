package n26.models;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransactionSummaryTest {

	@Test
	public void collectDataTest() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);

		TransactionSummary transactionSummary = new TransactionSummary(transaction.getTimestampInstant());
		transactionSummary.collectData(transaction);

		assertEquals(12.5, transactionSummary.getAvg(), 0);
		assertEquals(12.5, transactionSummary.getSum(), 0);
		assertEquals(12.5, transactionSummary.getMin(), 0);
		assertEquals(12.5, transactionSummary.getMax(), 0);
		assertEquals(1, transactionSummary.getCount(), 0);
	}

	@Test
	public void collectDataTestAndReset() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);

		TransactionSummary transactionSummary = new TransactionSummary(transaction.getTimestampInstant());
		transactionSummary.collectData(transaction);

		assertEquals(12.5, transactionSummary.getAvg(), 0);
		assertEquals(12.5, transactionSummary.getSum(), 0);
		assertEquals(12.5, transactionSummary.getMin(), 0);
		assertEquals(12.5, transactionSummary.getMax(), 0);
		assertEquals(1, transactionSummary.getCount(), 0);


		transactionSummary.reset(transaction.getTimestampInstant());
		assertEquals(0, transactionSummary.getAvg(), 0);
		assertEquals(0, transactionSummary.getSum(), 0);
		assertEquals(0, transactionSummary.getMin(), 0);
		assertEquals(0, transactionSummary.getMax(), 0);
		assertEquals(0, transactionSummary.getCount(), 0);
	}

	@Test
	public void collectDataTestAndResetAndVerifyCloneIsNotAffected() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);

		TransactionSummary transactionSummary = new TransactionSummary(transaction.getTimestampInstant());
		transactionSummary.collectData(transaction);

		assertEquals(12.5, transactionSummary.getAvg(), 0);
		assertEquals(12.5, transactionSummary.getSum(), 0);
		assertEquals(12.5, transactionSummary.getMin(), 0);
		assertEquals(12.5, transactionSummary.getMax(), 0);
		assertEquals(1, transactionSummary.getCount(), 0);

		TransactionSummary copy = transactionSummary.copy();
		transactionSummary.reset(transaction.getTimestampInstant());

		assertEquals(12.5, copy.getAvg(), 0);
		assertEquals(12.5, copy.getSum(), 0);
		assertEquals(12.5, copy.getMin(), 0);
		assertEquals(12.5, copy.getMax(), 0);
		assertEquals(1, copy.getCount(), 0);
	}

	@Test
	public void checkIfCreatedBeforeTest() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);

		TransactionSummary transactionSummary = new TransactionSummary(transaction.getTimestampInstant());
		assertTrue(transactionSummary.isSummaryCreatedBefore(transaction.getTimestampInstant().plusSeconds(2)));
	}
}