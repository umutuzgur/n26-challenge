package n26.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TransactionTest {

	@Test
	public void createTransactionTest() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);
		assertEquals(12.5, transaction.getAmount(), 0);
		assertEquals(1506796030000L, transaction.getTimestamp());
	}

	@Test
	public void changeValuesTest() throws Exception {
		Transaction transaction = new Transaction(12.5, 1506796030000L);
		transaction.setAmount(1);
		transaction.setTimestamp(1506793030000L);
		assertEquals(1, transaction.getAmount(), 0);
		assertEquals(1506793030000L, transaction.getTimestamp());
	}
}