package n26.services;

import n26.exceptions.OutdatedTransactionException;
import n26.models.Transaction;
import n26.models.TransactionSummary;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertEquals;

public class TransactionServiceTest {

	@Test
	public void processTransactionAndGetSummary() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction = new Transaction(12, now.toEpochMilli());
		service.processTransaction(transaction);
		TransactionSummary summary = service.getSummaryOfLast60Seconds(now);
		assertEquals(12.0, summary.getAvg(), 0);
		assertEquals(12.0, summary.getMax(), 0);
		assertEquals(12.0, summary.getMin(), 0);
		assertEquals(12.0, summary.getSum(), 0);
		assertEquals(1, summary.getCount());
	}

	@Test
	public void process2TransactionsGetSummary() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction1 = new Transaction(12, now.toEpochMilli());
		Transaction transaction2 = new Transaction(16, now.plusSeconds(1).toEpochMilli());
		service.processTransaction(transaction1);
		service.processTransaction(transaction2);
		TransactionSummary summary = service.getSummaryOfLast60Seconds(now.plusSeconds(1));
		assertEquals(14.0, summary.getAvg(), 0);
		assertEquals(16.0, summary.getMax(), 0);
		assertEquals(12.0, summary.getMin(), 0);
		assertEquals(28.0, summary.getSum(), 0);
		assertEquals(2, summary.getCount());
	}

	@Test
	public void process3TransactionsGetSummary() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction1 = new Transaction(12, now.toEpochMilli());
		Transaction transaction2 = new Transaction(16, now.plusSeconds(1).toEpochMilli());
		Transaction transaction3 = new Transaction(20, now.plusSeconds(3).toEpochMilli());
		service.processTransaction(transaction1);
		service.processTransaction(transaction2);
		service.processTransaction(transaction3);
		TransactionSummary summary = service.getSummaryOfLast60Seconds(now.plusSeconds(3));
		assertEquals(16.0, summary.getAvg(), 0);
		assertEquals(20.0, summary.getMax(), 0);
		assertEquals(12.0, summary.getMin(), 0);
		assertEquals(48.0, summary.getSum(), 0);
		assertEquals(3, summary.getCount());
	}

	@Test
	public void ignoreFirstTransactionGetSummary() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction1 = new Transaction(12, now.toEpochMilli());
		Transaction transaction2 = new Transaction(16, now.plusSeconds(1).toEpochMilli());
		Transaction transaction3 = new Transaction(20, now.plusSeconds(3).toEpochMilli());
		service.processTransaction(transaction1);
		service.processTransaction(transaction2);
		service.processTransaction(transaction3);
		TransactionSummary summary = service.getSummaryOfLast60Seconds(now.plusSeconds(61));
		assertEquals(18.0, summary.getAvg(), 0);
		assertEquals(20.0, summary.getMax(), 0);
		assertEquals(16.0, summary.getMin(), 0);
		assertEquals(36.0, summary.getSum(), 0);
		assertEquals(2, summary.getCount());
	}


	@Test
	public void getSummaryForOldTransactionsTransaction() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction = new Transaction(12, now.toEpochMilli());
		service.processTransaction(transaction);
		TransactionSummary summary = service.getSummaryOfLast60Seconds(now.plusSeconds(120));
		assertEquals(0.0, summary.getAvg(), 0);
		assertEquals(0.0, summary.getMax(), 0);
		assertEquals(0.0, summary.getMin(), 0);
		assertEquals(0.0, summary.getSum(), 0);
		assertEquals(0, summary.getCount());
	}

	@Test(expected = OutdatedTransactionException.class)
	public void processOldTransaction() throws Exception {
		Instant now = Instant.now();
		TransactionService service = new TransactionService();
		Transaction transaction = new Transaction(12, now.minusSeconds(120).toEpochMilli());
		service.processTransaction(transaction);
	}

}